// this is for gettid()
#define _GNU_SOURCE	

/* System headers. */
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <errno.h>
#include <stdint.h>
#include <string.h>

#include "FreeRTOS.h"

#include "task.h"
#include "queue.h"
#include "croutine.h"

#include <unistd.h>
#include <signal.h>
#include <linux/types.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "bbbgpio_ioctl.h"

// with FreeRTOS tasks we need the thread ID (not PID)
#define gettid()	syscall(SYS_gettid)

#define PIN1	69
#define PIN2	66

int fd;
struct bbbgpio_ioctl_struct
{
	uint8_t         gpio_number;
	unsigned int    value;
};
struct bbbgpio_ioctl_struct ioctl_struct;
volatile int irq_count1 = 0;
volatile int irq_count2 = 0;
int SIGRTIRQ = 0;

void task1 (void *unused);
void task2 (void *unused);
void receive_data(int n, siginfo_t * info, void *unused);
void ctrl_c_handler (int sig);

int main( void )
{
    xTaskHandle taskHandle1;
    xTaskHandle taskHandle2;

	if (signal(SIGINT, (void *)ctrl_c_handler) == SIG_ERR) {
		printf("\ncan't catch SIGINT\n");
		return -1;
	}

	fd = open("/dev/bbbgpio0", O_RDWR);
	if(fd == -1){
		printf("Open:%s\n", strerror(errno));
		exit (-2);
	}

	ioctl_struct.value = 0;		// direction IN
	
	// GPIO PIN1
	ioctl_struct.gpio_number = PIN1;
	printf ("Requesting pin%d\n", ioctl_struct.gpio_number);
	if(ioctl(fd, PIN_REG, &ioctl_struct) != 0) {
			printf("Pin %d PIN_REG: %s\n", ioctl_struct.gpio_number, strerror(errno));
	}
	printf ("Setting direction IN\n");
	if(ioctl(fd, PIN_SETDIR, &ioctl_struct) != 0) {
			printf("Pin %d PIN_SETDIR: %s\n", ioctl_struct.gpio_number, strerror(errno));
	}
	printf ("Setting interrupt at rising edge\n");
	if(ioctl(fd, PIN_RISEDGE, &ioctl_struct) != 0) {
			printf("Pin %d PIN_RISEDGE: %s\n", ioctl_struct.gpio_number, strerror(errno));
	}
	if(ioctl(fd, PIN_ENINT, &ioctl_struct) != 0) { 
			printf("Pin %d PIN_ENINT: %s\n", ioctl_struct.gpio_number, strerror(errno));                                        
	}
	printf ("Irq number for Pin %d: %d\n", ioctl_struct.gpio_number, ioctl_struct.value);

	// GPIO PIN2
	ioctl_struct.gpio_number = PIN2;
	printf ("Requesting pin%d\n", ioctl_struct.gpio_number);
	if(ioctl(fd, PIN_REG, &ioctl_struct) != 0) {
			printf("Pin %d PIN_REG: %s\n", ioctl_struct.gpio_number, strerror(errno));
	}
	printf ("Setting direction IN\n");
	if(ioctl(fd, PIN_SETDIR, &ioctl_struct) != 0) {
			printf("Pin %d PIN_SETDIR: %s\n", ioctl_struct.gpio_number, strerror(errno));
	}
	printf ("Setting interrupt at rising edge\n");
	if(ioctl(fd, PIN_RISEDGE, &ioctl_struct) != 0) {
			printf("Pin %d PIN_RISEDGE: %s\n", ioctl_struct.gpio_number, strerror(errno));
	}
	if(ioctl(fd, PIN_ENINT, &ioctl_struct) != 0) { 
			printf("Pin %d PIN_ENINT: %s\n", ioctl_struct.gpio_number, strerror(errno));                                        
	}
	printf ("Irq number for Pin %d: %d\n", ioctl_struct.gpio_number, ioctl_struct.value);

	// Get the signal number used by bbbgpio driver
	if(ioctl(fd, VAL_GETSIG, &SIGRTIRQ) != 0) { 
			printf("Cannot get signal number: %s\n", strerror(errno));                                        
	}
	else {
		if (SIGRTIRQ < SIGRTMIN) {	// userland and kernel SIGRTMIN are different!
			printf("Signal number from driver is %d, but the local SIGRTMIN is %d\n", SIGRTIRQ, SIGRTMIN); 
			SIGRTIRQ = SIGRTMIN + 1;
			printf("Trying to set a different number %d\n", SIGRTIRQ); 
			if(ioctl(fd, VAL_SETSIG, &SIGRTIRQ) != 0) { 
					printf("Cannot set signal number: %s\n", strerror(errno));                                        
			}
		}
		printf("Signal number for SIGRTIRQ: %d\n", SIGRTIRQ); 
	}

	xTaskCreate( task2, "TASK2", 1000, 0, 2, &taskHandle2 );
	xTaskCreate( task1, "TASK1", 1000, 0, 2, &taskHandle1 );

	/* Set the scheduler running.  This function will not return unless a task calls vTaskEndScheduler(). */
	vTaskStartScheduler();

	return 1;
}

void receive_data_1(int n, siginfo_t * info, void *unused) {
    if (info->si_int == PIN1)
		irq_count1++;
	else
		irq_count2++;
}

void ctrl_c_handler (int sig) {

    //signal(sig, SIG_IGN);
    printf ("\nCtrl-C is detected, releasing pins\n");

	ioctl_struct.gpio_number = PIN1;
    if(ioctl(fd, PIN_DISINT, &ioctl_struct) != 0) {
            printf("Pin %d: PIN_DISINT: %s\n", ioctl_struct.gpio_number, strerror(errno));
    }
    if(ioctl(fd, PIN_UNREG, &ioctl_struct) != 0) {
            printf("Pin %d PIN_UNREG: %s\n", ioctl_struct.gpio_number, strerror(errno));
    }

	ioctl_struct.gpio_number = PIN2;
    if(ioctl(fd, PIN_DISINT, &ioctl_struct) != 0) {
            printf("Pin %d: PIN_DISINT: %s\n", ioctl_struct.gpio_number, strerror(errno));
    }
    if(ioctl(fd, PIN_UNREG, &ioctl_struct) != 0) {
            printf("Pin %d PIN_UNREG: %s\n", ioctl_struct.gpio_number, strerror(errno));
    }

    close(fd);

    exit (1);
}

void task1 (void *unused)

{
	int count = 0;

	struct sigaction sig;
	sig.sa_sigaction = receive_data_1;
	sig.sa_flags = SA_SIGINFO | SA_RESTART;
	sigfillset(&sig.sa_mask);	// block other signals while processing the interrupt
	sigaction(SIGRTIRQ, &sig, NULL);
	printf("Task1 SIGRTIRQ handler: %ld\n", sig.sa_sigaction);

	int pid = gettid();
	//int pid = getpid();
	if(ioctl(fd, VAL_SENDPID, &pid) != 0) {
		printf("Task1 VAL_PID: %s\n", strerror(errno));
	}
	else
		printf ("Task1 PID is sent to kernel: %d\n", pid);

	while (1) {
		printf ("Task1: %d\n", ++count);
		for (int i = 0 ; i < 100 ; i++)
			vTaskDelay (50);

		printf ("Task1 done: %d\n", count);
	}
}

void task2 (void *unused)

{
	int count = 0;

	while (1) {
		printf ("Task2: %d\n", ++count);
		for (int i = 0 ; i < 100 ; i++)
			vTaskDelay (60);
		printf ("Task2: done %d\n", count);
		printf ("IRQ count: Pin %d: %d, Pin %d: %d\n", PIN1, irq_count1, PIN2, irq_count2);
		
	}
}

