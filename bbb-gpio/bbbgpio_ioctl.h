/* please include this header file in your userland program */

#ifndef BBBGPIO_IOCTL_H_
#define BBBGPIO_IOCTL_H_

#define MAX_INT_PINS    16
#define SIGRT35         35

/* ioctl structure */
struct bbb_ioctl_struct
{
    uint8_t pin_or_port_num;
    unsigned int value;
};

struct bbb_irq_counter {
    uint64_t pin_in_ms_out;     // absolute timestamp in milliseconds
    uint64_t count;             // pulse count
};

struct bbb_irq_counter_currtime {
    uint64_t pin_in_ms_out;     	// absolute timestamp in milliseconds
    uint64_t pulse_count;           // pulse count
    uint64_t currtime_in_ms_out;    // current time in milliseconds
};

struct bbb_spi_msg {
	uint8_t spi_port;
	uint8_t spi_msg[];
};

/*====================================
  DRIVER's IOCTL OPTIONS
====================================*/
#define _IOCTL_MAGIC 'K'

/* pairs of pin number / value */
#define PIN_REG                _IOW(_IOCTL_MAGIC,1,struct bbb_ioctl_struct*)            /* IOCTL used for registering PIN */
#define PIN_UNREG              _IOW(_IOCTL_MAGIC,2,struct bbb_ioctl_struct*)            /* IOCTL used for unregistering PIN */
#define PIN_RESERVED           _IOR(_IOCTL_MAGIC,3,struct bbb_ioctl_struct*)            /* not used anymore */
#define PIN_READBUF            _IOR(_IOCTL_MAGIC,3,struct bbb_ioctl_struct*)            /* read from register */
#define PIN_SETDIR             _IOW(_IOCTL_MAGIC,4,struct bbb_ioctl_struct*)            /* set direction */
#define PIN_RISEDGE            _IOW(_IOCTL_MAGIC,5,struct bbb_ioctl_struct*)            /* set rising edge */
#define PIN_FALEDGE            _IOW(_IOCTL_MAGIC,6,struct bbb_ioctl_struct*)            /* set falling edge */
#define PIN_ENINT              _IOW(_IOCTL_MAGIC,7,struct bbb_ioctl_struct*)            /* enable gpio interrupt, set debounce delay in ms */
#define PIN_DISINT             _IOW(_IOCTL_MAGIC,8,struct bbb_ioctl_struct*)            /* disable gpio interrupt */
#define PIN_RISEDGE_NO_SIGNAL  _IOW(_IOCTL_MAGIC,9,struct bbb_ioctl_struct*)            /* set rising edge interrupt, but don't trigger a signal */
#define PIN_FALEDGE_NO_SIGNAL  _IOW(_IOCTL_MAGIC,10,struct bbb_ioctl_struct*)           /* set falling edge interrupt, but don't trigger a signal */
#define PIN_READ_ONE_PORT      _IOW(_IOCTL_MAGIC,11,struct bbb_ioctl_struct*)           /* read gpio0, gpio1, gpio2, or gpio3 into "value".  gpio_number is 0, 1, 2, or 3. */
#define PIN_VALID_PERIOD       _IOW(_IOCTL_MAGIC,12,struct bbb_ioctl_struct*)           /* detected pulse is valid when it's longer than this period (times of VAL_TIMER_INTERVAL) */
#define PIN_RES_IRQCOUNT       _IOW(_IOCTL_MAGIC,13,struct bbb_ioctl_struct*)           /* reset IRQ counts for pin */
/* ... */                                                                             
#define PIN_MAX                _IOW(_IOCTL_MAGIC,19,struct bbb_ioctl_struct*)           /* max number for pin ioctl */
                                                                                      
/* integer values */                                                                  
#define VAL_SENDPID            _IOW(_IOCTL_MAGIC,20,int *)                              /* get PID form user program to send signal to */
#define VAL_GETSIG             _IOW(_IOCTL_MAGIC,21,int *)                              /* get signal number used by driver */ 
#define VAL_SETSIG             _IOW(_IOCTL_MAGIC,22,int *)                              /* set signal number to be used by driver */ 
#define VAL_RELINTS            _IOW(_IOCTL_MAGIC,23,int *)                    	        /* disable all interrupts */
#define VAL_GETINTS            _IOW(_IOCTL_MAGIC,24,int *)    		        	        /* get list of interrupts */
#define VAL_GET_UNCOUNTED      _IOW(_IOCTL_MAGIC,25,unsigned int *)	        	        /* get the number of uncounted irqs */
#define VAL_DRVVER             _IOW(_IOCTL_MAGIC,26,int *)    		        	        /* get driver version to check for compatibilty */
#define VAL_DEBUG              _IOW(_IOCTL_MAGIC,27,int *)                              /* turn on/off system debug messages */
#define VAL_SIGONOFF           _IOW(_IOCTL_MAGIC,28,int *)                              /* turn on/off sending signals */
#define VAL_TIMER_INTERVAL     _IOW(_IOCTL_MAGIC,29,int *)                              /* detected pulses difgitization timer period in ms */
                                                                                      
/* miscellaneous */                                                                   
#define SYS_TIME               _IOW(_IOCTL_MAGIC,40,time_t *)                           /* set system time in seconds */
#define GET_IRQ_COUNT_CURRTIME _IOW(_IOCTL_MAGIC,42,struct bbb_irq_counter_currtime*)   /* get timestamp, pulse count and current time */
#define SEND_SPI_MSG           _IOW(_IOCTL_MAGIC,43,struct bbb_spi_msg*)                /* send a spi message - for testing purpose */
#define DISABLE_ALLINTS        _IOW(_IOCTL_MAGIC,44,void *) 					        /* disable all gpio interrupts */

#endif

