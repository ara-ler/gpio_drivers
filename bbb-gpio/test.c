#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <linux/types.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include "bbbgpio_ioctl.h"


int fd;
int gpio_num;
int int_count = 0;
struct bbbgpio_ioctl_struct
{
    uint8_t 		gpio_number;
    unsigned int 	value;
};
struct bbbgpio_ioctl_struct ioctl_struct;
int SIGRTIRQ = 0;


void ctrl_c_handler (int sig) {
	
	ioctl_struct.gpio_number = gpio_num;
	printf ("\nCtrl-C is detected, releasing pin %d\n", ioctl_struct.gpio_number);
	if(ioctl(fd, PIN_DISINT, &ioctl_struct) != 0) {
            fprintf(stderr,"IOC_DISINT: %s\n", strerror(errno));
	}
	if(ioctl(fd, PIN_UNREG, &ioctl_struct) != 0) {
            fprintf(stderr,"IOC_UNREG: %s\n", strerror(errno));
	}
	close(fd);
	exit (1);
}

void receive_data(int n, siginfo_t * info, void *unused) {
	// printf ("received value %i\n", info->si_int); // printf() is not a safe function to be used in a signal handler
	int_count++;
}

int main(int argc, char **argv)
{
	int i;
	int read_value;
	int irq;
	
	if (argc < 2) {
		printf ("Usage: %s <gpio_num>\n", argv[0]);
		exit (-1);

	}

	if (signal(SIGINT, (void *)ctrl_c_handler) == SIG_ERR) {
		printf("\ncan't catch SIGINT\n");
		return -1;
	}

	fd = open("/dev/bbbgpio0", O_RDWR);
	if(fd == -1){
		fprintf(stderr,"Open:%s\n", strerror(errno));
		exit (-2);
	}

	gpio_num= atoi(argv[1]);
	ioctl_struct.gpio_number = gpio_num;

	printf ("Requesting pin %d\n", ioctl_struct.gpio_number);
	if(ioctl(fd, PIN_REG, &ioctl_struct) != 0) {
            fprintf(stderr,"IOC_REG: %s\n", strerror(errno));
	}

	printf ("Setting direction IN\n");
	ioctl_struct.value = 0;

	if(ioctl(fd, PIN_SETDIR, &ioctl_struct) != 0) {
            fprintf(stderr,"IOC_SETDIR: %s\n", strerror(errno));
	}

	int pid = getpid();
	if(ioctl(fd, VAL_SENDPID, &pid) != 0) {
            fprintf(stderr,"IOC_PID: %s\n", strerror(errno));
	}
	else
		printf ("PID is sent to kernel: %d\n", pid);

	printf ("Reading pin %d\n", ioctl_struct.gpio_number);
	read(fd, &ioctl_struct, sizeof(struct bbbgpio_ioctl_struct));
	printf("0x%08X\n", ioctl_struct.value);
	
	printf ("Setting interrupt at rising edge\n");
	if(ioctl(fd, PIN_RISEDGE, &ioctl_struct) != 0) {
            fprintf(stderr,"IOC_RISEDGE: %s\n", strerror(errno));
	}
	
	if(ioctl(fd, PIN_ENINT, &ioctl_struct) != 0) {
            fprintf(stderr,"IOC_ENINT: %s\n", strerror(errno));
	}

	printf ("Irq number: %d\n", ioctl_struct.value);

	// Get the signal number used by bbbgpio driver
	if(ioctl(fd, VAL_GETSIG, &SIGRTIRQ) != 0) { 
			printf("Cannot get signal number: %s\n", strerror(errno));                                        
	}
	else {
		if (SIGRTIRQ < SIGRTMIN) {	// userland and kernel SIGRTMIN are different!
			printf("Signal number from driver is %d, but the local SIGRTMIN is %d\n", SIGRTIRQ, SIGRTMIN); 
			SIGRTIRQ = SIGRTMIN + 1;
			printf("Trying to set a different number %d\n", SIGRTIRQ); 
			if(ioctl(fd, VAL_SETSIG, &SIGRTIRQ) != 0) { 
					printf("Cannot set signal number: %s\n", strerror(errno));                                        
			}
		}
		printf("Signal number for SIGRTIRQ: %d\n", SIGRTIRQ); 
	}

	struct sigaction sig;
	sig.sa_sigaction = receive_data;
	sig.sa_flags = SA_SIGINFO | SA_RESTART;
	sigfillset (&sig.sa_mask);   	// block other signals while processing the interrupt
	sigaction (SIGRTIRQ, &sig, NULL);

	while (1) {
		printf ("Interrupts count: %d\n", int_count);
		sleep (1);
	}
}

