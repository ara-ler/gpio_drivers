This is a fork of original repository:
https://github.com/carhero/bbb_drivers

This is a driver for BeagleBone Black GPIOs.
It allows to set up pins' direction, read/write values and enable interrupts.

The interrupts capture functionality is the main purpose of this project. In contrast of the traditional poll() and epoll() mechanisms (which consume valuable resources of the BBB's CPU), this driver simply sends a real-time signal to the user application when an interrupt request occurs.

Also 64-bit interrupt counters are added with the corresponding IOCTLs.

